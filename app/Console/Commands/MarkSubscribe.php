<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class MarkSubscribe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mark:subscribed {yes=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::table('followers')->chunkById(100, function ($followers) {
            foreach ($followers as $follower) {
                DB::table('followers')
                    ->where('id', $follower->id)
                    ->update(['is_signed' => $this->argument('yes'), 'sync_at' => now()]);
            }
        });
    }
}
