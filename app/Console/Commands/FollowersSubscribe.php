<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;
use NZTim\Mailchimp\Mailchimp;
use Illuminate\Console\Command;

class FollowersSubscribe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:followers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send followers to mailchimp if they subscribed';

    /**
     * FollowersSubscribe constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::table('followers')->chunkById(100, function ($followers) {
            $key = config('services.mc_key');
            $list_id = config('services.mc_key_list');
            $mc = new Mailchimp($key);

            foreach ($followers as $follower) {
                if($follower->is_signed) {
                    try {
                        $mc->subscribe($list_id, $follower->email, $merge = [], $confirm = true);
                    } catch (\Throwable $e) {
                        report($e);
                    }
                } else {
                    if($mc->check($list_id, $follower->email)) {
                        $mc->unsubscribe($list_id, $follower->email);
                    }
                }
            }
        });
    }
}
