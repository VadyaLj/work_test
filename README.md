1) run 'composer install'
2) set your db name, dbuser in .env
3) run "php artisan migrate:fresh --seed"

4) start server "php artisan serve" 

5) auth : 
    login : dudenko.vadim@gmail.com
    password: 12345

6) console programs:
    a) 
    "php artisan mark:subscribed" - by default signed all email to subscribers 
    "php artisan mark:subscribed 0" - remove all email to subscribers
    b) 
        "php artisan send:followers" - Checks email subscription and add/remove from mailchimp
        
7) 6.a added to schedule how to start with cron or laravel view the documentation https://laravel.com/docs/8.x/scheduling        
        
        