<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes(['register' => false, 'reset' => false, 'confirm' => false]);

Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);

Route::post('/', [App\Http\Controllers\HomeController::class, 'store'])->name('save_follower');